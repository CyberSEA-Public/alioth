# Alioth 🔐

Alioth is a simple web app that can be used to determine if a security metric is sound.

The soundness evaluation is based off of the paper *Improving the Derivation of Sound Security Metrics* by Dr. George O.M. Yee. You can read the paper [here](https://ieeexplore.ieee.org/abstract/document/9842489).

Created by the CyberSEA Lab at Carleton University. Learn more [here](https://carleton.ca/cybersea/).

### Did You Know?
Alioth is a star in the Ursa Major constellation. It is the 31st brightest star in the sky and is the star in the Big Dipper's handle closest to the bowl. Alioth follows the naming conventions of previous released security posture analysis tools. You can learn more about these tools [here](https://compass.carleton.ca/explore).

## Development

### Tools

- Code editor: [PyCharm](https://www.jetbrains.com/pycharm/download/)
- Python: [3.9+](https://www.python.org/downloads/)
- Python package manager: [requirements.txt](https://www.jetbrains.com/help/pycharm/managing-dependencies.html)

### Getting Started

1. Clone the repository
2. **Option 1** - IDE
   1. Open the Project's root directory within a Python supported IDE, such as PyCharm or VSCode
   2. Navigate to `alioth/main.py`
   3. Run the main method of the application within main.py. By default, the app should be launched on localhost:5000 (which you can navigate to into your web browser of choice).
3. **Option 2** - CLI
   1. Navigate to the root directory of the project via your command line of choice.
   2. Ensure Python 3.9+ is installed on your system. If it is not, install it from the above download link.
      1. On Windows, use the command `py` to check for a Python installation
      2. On OSX and Linux, use the command `python3 --version`
   3. From the root directory of the project, install the required dependencies using the command `pip install -r requirements.txt`
   4. Run the tool using the command `py main.py`. By default, the app should be launched on localhost:5000 (which you can navigate to into your web browser of choice).

## Soundness Evaluation

Alioth is meant to evaluate the soundness of security metrics. To that end, the main analysis page of Alioth requires users to answer 6-7 yes or no questions. Depending on the responses, Alioth will let you know if the metric in question is sound and whether it satisfies the conditions of being well-defined, progressive, and reproducible.

**More details on how this analysis works can be found within the source code of Alioth, specifically within the HTML templates**.

## Known issues

Currently, there are no known issues.

> If you notice a bug, please add it to Issues tab. Make sure you include how to recreate the bug!

