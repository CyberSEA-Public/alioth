from flask import Flask, render_template

app = Flask(__name__)


# Defining the home page of our site
@app.route("/")  # this sets the route to this page
def home():
    return render_template("index.html")


# Defining the learn page of our site
@app.route("/learn")
def learn_page():
    return render_template("learn.html")


# Defining the learn page of our site
@app.route("/analysis")
def analysis_page():
    return render_template("analysis.html")


# Entry point for the application.
if __name__ == "__main__":
    app.run()
